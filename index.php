<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Stats</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>


    <?php include 'includes/PDO.php'?>


    <div class="container-fluid">
        <div class="row navTop">
            <button class="button" data-open="NewGame" onclick="openModalNewGame()">une game?</button>
        </div>
        <div class="row">
            <table style="text-align: center;" id="table">
                <tr>
                    <th>Numéros de Partie</th>
                    <th>Kill</th>
                    <th>Death</th>
                    <th>Ratio</th>
                    <th>Heros</th>      
                </tr>
                <?php 
                    $reponse = $bdd->query('SELECT * FROM stat');
                    while ($donnees = $reponse->fetch())
                    {
                ?>  
                <tr>
                    <td><?php echo $donnees['id']; ?></td>
                    <td><?php echo $donnees['kills']; ?></td>
                    <td><?php echo $donnees['death']; ?></td>
                    <td><?php echo $donnees['ratio']; ?></td>
                    <td><?php echo $donnees['hero']; ?></td>
                </tr>
                    <?php }?>
            </table>
        </div>
    </div>

    <div id="NewGame" class="modal reveal" data-reveal>
        <form action="traitement/Enregistrement.php" id="form" method="post">
            <fieldset>
                <p>
                    <label>Kill</label>
                    <input name="kills">
                </p>
                <p>
                    <label>Death</label>
                    <input name="death">
                </p>
                <p>
                    <label>Hero</label>
                    <input name="hero">
                </p>
                <p>
                    <input type="submit" value="Save">
                </p>
            </fieldset>
        </form>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>

    <script>
        function openModalNewGame() {
            $('#NewGame').foundation('open');
        }

        // setInterval(function test(){ 
        //     $.ajax({
        //         url : 'index.php',// La ressource ciblée
        //         type: 'POST' })
        // },3000);

        setInterval(function () {
            $('#table').load('traitement/AjaxTable.php', '', cb_Foundation);
        }, 1000);

        function cb_Foundation() {
            Foundation.reInit('table');
        }
    </script>
</body>
</html>